//
//  VideoRepository.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Combine

public protocol VideoRepositoryProtocol {
    func fetchVideos(isFavorite: Bool) -> AnyPublisher<[VideoEntity], Error>
    func changeFavorite(for video: VideoEntity) -> AnyPublisher<Bool, Error>
}

public class VideoRepository: VideoRepositoryProtocol {
    let remoteDataSource: VideoRemoteDataSourceProtocol
    let localDataSource: VideoLocalDataSourceProtocol
    
    public init(remoteDataSource: VideoRemoteDataSourceProtocol = VideoRemoteDataSource(),
         localDataSource: VideoLocalDataSourceProtocol = VideoLocalDataSource()) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    public func fetchVideos(isFavorite: Bool) -> AnyPublisher<[VideoEntity], Error> {
        return remoteDataSource.getVideos()
            .flatMap { response -> AnyPublisher<[VideoEntity], Error> in
                self.localDataSource.saveVideos(responses: response)
                return self.localDataSource.loadVideos(isFavorite: isFavorite)
            }
            .catch{ _ in
                return self.localDataSource.loadVideos(isFavorite: isFavorite)
            }
            .eraseToAnyPublisher()
    }
    
    public func changeFavorite(for video: VideoEntity) -> AnyPublisher<Bool, Error> {
        return localDataSource.changeFavorite(for: video)
    }
}
