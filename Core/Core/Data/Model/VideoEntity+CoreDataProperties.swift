//
//  VideoEntity+CoreDataProperties.swift
//  
//
//  Created by Rizal Hidayat on 12/05/24.
//
//

import Foundation
import CoreData


extension VideoEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VideoEntity> {
        return NSFetchRequest<VideoEntity>(entityName: "VideoEntity")
    }

    @NSManaged public var author: String?
    @NSManaged public var duration: String?
    @NSManaged public var id: String?
    @NSManaged public var isFavorite: Bool
    @NSManaged public var subscriber: String?
    @NSManaged public var thumbnailUrl: String?
    @NSManaged public var title: String?
    @NSManaged public var uploadTime: String?
    @NSManaged public var videoDescription: String?
    @NSManaged public var videoUrl: String?
    @NSManaged public var views: String?

}
