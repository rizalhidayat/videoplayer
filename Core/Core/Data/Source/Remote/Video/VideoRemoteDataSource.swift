//
//  VideoRemoteDataSource.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Combine

public protocol VideoRemoteDataSourceProtocol {
    func getVideos() -> AnyPublisher<[VideoResponse], Error>
}

public class VideoRemoteDataSource: VideoRemoteDataSourceProtocol {
    public init() { }
    public func getVideos() -> AnyPublisher<[VideoResponse], Error> {
        return ApiService.shared.loadAndDecodeJSON(fileName: "videos")
    }
}
