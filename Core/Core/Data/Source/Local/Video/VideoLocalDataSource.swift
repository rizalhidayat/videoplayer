//
//  VideoLocalDataSource.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Combine

public protocol VideoLocalDataSourceProtocol {
    func loadVideos(isFavorite: Bool) -> AnyPublisher<[VideoEntity], Error>
    func saveVideos(responses: [VideoResponse])
    func changeFavorite(for video: VideoEntity) -> AnyPublisher<Bool, Error>
}

public class VideoLocalDataSource: VideoLocalDataSourceProtocol {
    public init() { }
    private let dbService = DBService.shared
    
    public func saveVideos(responses videos: [VideoResponse]) {
        dbService.context.performAndWait {
            for video in videos {
                do {
                    let fetchRequest = VideoEntity.fetchRequest()
                    fetchRequest.predicate = NSPredicate(format: "id == %@", video.id)
                    
                    let existingVideos = try dbService.context.fetch(fetchRequest)
                    
                    if let existingVideo = existingVideos.first {
                        existingVideo.title = video.title
                        existingVideo.thumbnailUrl = video.thumbnailUrl
                        existingVideo.duration = video.duration
                        existingVideo.uploadTime = video.uploadTime
                        existingVideo.views = video.views
                        existingVideo.author = video.author
                        existingVideo.videoUrl = video.videoUrl
                        existingVideo.videoDescription = video.description
                        existingVideo.subscriber = video.subscriber
                        
                    } else {
                        let newVideoEntity = VideoEntity(context: dbService.context)
                        newVideoEntity.id = video.id
                        newVideoEntity.title = video.title
                        newVideoEntity.thumbnailUrl = video.thumbnailUrl
                        newVideoEntity.duration = video.duration
                        newVideoEntity.uploadTime = video.uploadTime
                        newVideoEntity.views = video.views
                        newVideoEntity.author = video.author
                        newVideoEntity.videoUrl = video.videoUrl
                        newVideoEntity.videoDescription = video.description
                        newVideoEntity.subscriber = video.subscriber
                        newVideoEntity.isFavorite = false
                    }
                    
                    try dbService.saveContext()
                } catch {
                    print("Error saving genre: \(error)")
                }
            }
        }
    }
    
    public func loadVideos(isFavorite: Bool) -> AnyPublisher<[VideoEntity], Error> {
        dbService.context.performAndWait {
            do {
                let fetchRequest = VideoEntity.fetchRequest()
                if isFavorite {
                    fetchRequest.predicate = NSPredicate(format: "isFavorite == %@", NSNumber(value: isFavorite))
                }
                let videos = try dbService.context.fetch(fetchRequest)
                return Just(videos)
                    .setFailureType(to: Error.self)
                    .eraseToAnyPublisher()
            } catch {
                return Fail(error: error)
                    .eraseToAnyPublisher()
            }
        }
    }
    
    public func changeFavorite(for video: VideoEntity) -> AnyPublisher<Bool, Error> {
        dbService.context.performAndWait {
            do {
                video.isFavorite.toggle()
                try dbService.saveContext()
                return Just(video.isFavorite)
                    .setFailureType(to: Error.self)
                    .eraseToAnyPublisher()
            } catch {
                return Fail(error: error)
                    .eraseToAnyPublisher()
            }
        }
    }
    
}
