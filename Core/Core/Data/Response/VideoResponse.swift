//
//  VideoResponse.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation

public struct VideoResponse: Decodable {
    let id: String
    let title: String
    let thumbnailUrl: String
    let duration: String
    let uploadTime: String
    let views: String
    let author: String
    let videoUrl: String
    let description: String
    let subscriber: String
}
