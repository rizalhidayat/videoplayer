//
//  UIImageView+Kingfisher.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit
import Kingfisher

extension UIImageView {
    public func setImage(path: String) {
        self.kf.indicatorType = .activity
        if let url = URL(string: path) {
            self.kf.setImage(with: url)
        }
    }
}
