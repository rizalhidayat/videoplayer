//
//  DBService.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import CoreData

class DBService {
    static let shared = DBService()
    
    private init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        let bundle = Bundle(for: DBService.self)
        guard let modelURL = bundle.url(forResource: "VideoPlayer", withExtension: "momd") else {
            fatalError("Failed to locate data model file in specified bundle.")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Failed to initialize managed object model from: \(modelURL)")
        }
        
        let container = NSPersistentContainer(name: "Payment", managedObjectModel: managedObjectModel)
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func saveContext() throws {
        guard context.hasChanges else { return }
        
        do {
            try context.save()
        } catch {
            throw error
        }
    }
}

