//
//  ApiService.swift
//  Core
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Combine

class ApiService {
    static let shared = ApiService()
    
    private init() {}
    
    func loadAndDecodeJSON<T: Decodable>(fileName: String) -> AnyPublisher<T, Error> {
        let bundle = Bundle(for: ApiService.self)
        guard let url = bundle.url(forResource: fileName, withExtension: "json") else {
            let error = NSError(domain: "ApiService", code: 404, userInfo: [NSLocalizedDescriptionKey: "File not found"])
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .mapError { $0 as Error }
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
