//
//  SceneDelegate.swift
//  VideoPlayer
//
//  Created by Rizal Hidayat on 11/05/24.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var mainWindow: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        mainWindow = window
        let tabBarVC = MainTabBarController()
        mainWindow?.rootViewController = tabBarVC.makeTabBar()
        mainWindow?.makeKeyAndVisible()
    }

}

