//
//  MainTabViewController.swift
//  VideoPlayer
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit
import Video

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func makeNavigation(viewController: UIViewController) -> UINavigationController {
        let navigation = UINavigationController(rootViewController: viewController)
        navigation.delegate = self
        navigation.navigationBar.prefersLargeTitles = false
        return navigation
    }
    
    func makeTabBar() -> MainTabBarController {
        let videoVC = VideoMainPageBuilder.makeVideoListPage()
        videoVC.title = "Video"
        videoVC.tabBarItem = UITabBarItem(title: "Video", image: UIImage(systemName: "video.fill"), tag: 0)
     
        let favoriteVC = VideoMainPageBuilder.makeFavoriteListPage()
        favoriteVC.title = "Favorite"
        favoriteVC.tabBarItem = UITabBarItem(title: "Favorite", image: UIImage(systemName: "heart.fill"), tag: 0)
     
       
        self.viewControllers = [
            makeNavigation(viewController: videoVC),
            makeNavigation(viewController: favoriteVC)
        ]
        return self
    }
    
}


extension MainTabBarController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.backButtonDisplayMode = .minimal
    }
}
