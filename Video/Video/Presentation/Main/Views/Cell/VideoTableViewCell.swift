//
//  VideoTableViewCell.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit
import Core

class VideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with video: VideoEntity) {
        authorLabel.text = video.author
        titleLabel.text = video.title
        durationLabel.text = video.duration
        if let path = video.thumbnailUrl {
            thumbnailImageView.setImage(path: path)
        }
    }
    
}
