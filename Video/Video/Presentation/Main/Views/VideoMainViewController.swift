//
//  VideoMainViewController.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit
import Core
import Combine

class VideoMainViewController: UIViewController {
    @IBOutlet weak var videoTableView: UITableView!
    
    private let viewModel: VideoMainViewModel
    
    private lazy var dataSource = makeDataSource()
    private var cancellables = Set<AnyCancellable>()
    
    init(viewModel: VideoMainViewModel) {
        let bundle = Bundle(for: VideoMainViewController.self)
        let nibName = String(describing: VideoMainViewController.self)
        self.viewModel = viewModel
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchVideo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initSubscribers()
    }
    
    private func initTableView() {
        videoTableView.register(cellClass: VideoTableViewCell.self)
        videoTableView.delegate = self
    }
    
    private func initSubscribers() {
        viewModel.$videos.receive(on: DispatchQueue.main)
            .sink { [weak self] videos in
                self?.update(with: videos)
            }
            .store(in: &cancellables)
    }
    
    //MARK: - Navigation
    private func navigateToDetail(video: VideoEntity) {
        let vm = VideoDetailViewModel(video: video)
        let vc = VideoDetailViewController(viewModel: vm)
        vc.title = "Video Detail"
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - UITableviewDataSource
extension VideoMainViewController {
    private enum Section: CaseIterable {
        case videos
    }
    
    private func makeDataSource() -> UITableViewDiffableDataSource<Section, VideoEntity> {
        return UITableViewDiffableDataSource(tableView: videoTableView) { tableView, indexPath, video in
            let cell = tableView.dequeue(cellClass: VideoTableViewCell.self, forIndexPath: indexPath)
            cell.configure(with: video)
            return cell
        }
    }
    
    private func update(with videos: [VideoEntity], animate: Bool = true) {
        if videos.isEmpty {
            let placeholderView = EmptyStateView()
            placeholderView.setTitle("No Video Available")
            videoTableView.backgroundView = placeholderView
            videoTableView.separatorStyle = .none
        } else {
            // Hide placeholder view
            videoTableView.backgroundView = nil
            videoTableView.separatorStyle = .singleLine
        }
        DispatchQueue.main.async {
            var snapshot = NSDiffableDataSourceSnapshot<Section, VideoEntity>()
            snapshot.appendSections(Section.allCases)
            snapshot.appendItems(videos, toSection: .videos)
            self.dataSource.apply(snapshot, animatingDifferences: animate)
        }
    }
}

extension VideoMainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let video = viewModel.videos[indexPath.row]
        navigateToDetail(video: video)
    }
}
