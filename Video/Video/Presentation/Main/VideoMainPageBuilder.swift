//
//  VideoMainPageBuilder.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit

public class VideoMainPageBuilder {
    public static func makeVideoListPage() -> UIViewController {
        let vm = VideoMainViewModel(isFavorite: false)
        let vc = VideoMainViewController(viewModel: vm)
        return vc
    }
    
    public static func makeFavoriteListPage() -> UIViewController {
        let vm = VideoMainViewModel(isFavorite: true)
        let vc = VideoMainViewController(viewModel: vm)
        return vc
    }
}
