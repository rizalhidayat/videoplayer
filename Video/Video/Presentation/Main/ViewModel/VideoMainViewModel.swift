//
//  VideoMainViewModel.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Core
import Combine

class VideoMainViewModel {
    private let repository: VideoRepositoryProtocol
    private var cancellables = Set<AnyCancellable>()
    private let isFavorite: Bool
    @Published private(set) var videos: [VideoEntity] = []
    
    init(isFavorite: Bool, repository: VideoRepositoryProtocol = VideoRepository()) {
        self.repository = repository
        self.isFavorite = isFavorite
        fetchVideo()
    }
    
    func fetchVideo() {
        repository.fetchVideos(isFavorite: isFavorite)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                if case let .failure(error) = completion {
                    print(error.localizedDescription)
                }
            } receiveValue: { videos in
                self.videos = videos
            }.store(in: &cancellables)
    }
}
