//
//  VideoDetailViewController.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import UIKit
import AVKit
import Combine

class VideoDetailViewController: UIViewController {
    
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var uploadTimeLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var subscriberLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    private let playerViewController = AVPlayerViewController()
    private let player = AVPlayer()
    
    private let viewModel: VideoDetailViewModel
    private var cancellables = Set<AnyCancellable>()
    
    init(viewModel: VideoDetailViewModel) {
        let bundle = Bundle(for: VideoDetailViewController.self)
        let nibName = String(describing: VideoDetailViewController.self)
        self.viewModel = viewModel
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not supported!")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVideoPlayer()
        initVideoDetail()
        initButtons()
        initSubscribers()
    }
    
    private func initVideoPlayer() {
        guard let urlString = viewModel.video.videoUrl,
            let videoURL = URL(string: urlString) else {
            print("Invalid URL")
            return
        }
        
        let player = AVPlayer(url: videoURL)
        
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        addChild(playerViewController)
        playerViewController.view.frame = videoContainerView.bounds
        videoContainerView.addSubview(playerViewController.view)
        playerViewController.didMove(toParent: self)
    }
    
    private func initVideoDetail() {
        let video = viewModel.video
        titleLabel.text = video.title
        viewsCountLabel.text = video.views
        uploadTimeLabel.text = video.uploadTime
        authorLabel.text = video.author
        subscriberLabel.text = video.subscriber
        descriptionLabel.text = video.videoDescription
    }
    
    private func initButtons() {
        
        favoriteButton.addTarget(self, action: #selector(favoriteButtonTapped), for: .touchUpInside)
    }
    
    private func initSubscribers() {
        viewModel.$isFavorited.receive(on: DispatchQueue.main)
            .sink { [weak self] status in
                self?.updateFavoriteButton(status: status)
            }
            .store(in: &cancellables)
    }
    
    private func updateFavoriteButton(status: Bool) {
        let title = status ? "Remove from favorite" : "Add to favorite"
        favoriteButton.setTitle(title, for: .normal)
    }
    //MARK: - Listener
    @objc
    private func favoriteButtonTapped() {
        viewModel.favorite()
    }
    
}
