//
//  VideoDetailViewModel.swift
//  Video
//
//  Created by Rizal Hidayat on 12/05/24.
//

import Foundation
import Core
import Combine

class VideoDetailViewModel {
    private let repository: VideoRepositoryProtocol
    private var cancellables = Set<AnyCancellable>()
    let video: VideoEntity
    @Published private(set) var isFavorited: Bool
    
    init(video: VideoEntity, repository: VideoRepositoryProtocol = VideoRepository()) {
        self.video = video
        self.repository = repository
        self.isFavorited = video.isFavorite
    }
    
    func favorite() {
        repository.changeFavorite(for: video)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                if case let .failure(error) = completion {
                    print(error.localizedDescription)
                }
            } receiveValue: { status in
                self.isFavorited = status
            }.store(in: &cancellables)
    }
}
